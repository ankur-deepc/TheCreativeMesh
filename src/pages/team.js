import React from "react";
import { withRouteData } from "react-static";
//

export default withRouteData(({ teams }) => (
  <div id="chef" className="section wb">
    <div className="container-fluid">
      <div className="section-title text-center">
        <h3>Meet Our Team</h3>
        <p>
          We thanks for all our awesome testimonials! There are hundreds of our
          happy customers!{" "}
        </p>
      </div>

      <div className="row">
        {teams.map((team) => (
          <div className="col-md-3 col-sm-6" key={team.sys.id}>
            <div className="our-team">
              <img
                src={`${team.fields.photo.fields.file.url}?w=300&h=500&fit=fill`}
                alt=""
              />
              <div className="team-content">
                <h3 className="title">{team.fields.name}</h3>
                <span className="post">{team.fields.role}</span>
                <ul className="social-links">
                  {team.fields.github && (
                    <li>
                      <a href={`${team.fields.github}`} target="_blank">
                        <i className="fa fa-github"></i>{" "}
                      </a>
                    </li>
                  )}
                  {team.fields.facebook && (
                    <li>
                      <a href={`${team.fields.facebook}`} target="_blank">
                        <i className="fa fa-facebook"></i>{" "}
                      </a>
                    </li>
                  )}
                  {team.fields.twitter && (
                    <li>
                      <a href={`${team.fields.twitter}`} target="_blank">
                        <i className="fa fa-twitter"></i>{" "}
                      </a>
                    </li>
                  )}
                  {team.fields.linkedin && (
                    <li>
                      <a href={`${team.fields.linkedin}`} target="_blank">
                        <i className="fa fa-linkedin"></i>{" "}
                      </a>
                    </li>
                  )}
                  {team.fields.pinterest && (
                    <li>
                      <a href={`${team.fields.pinterest}`} target="_blank">
                        <i className="fa fa-pinterest-p"></i>{" "}
                      </a>
                    </li>
                  )}
                </ul>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  </div>
));

import React from "react";
import { useRouteData } from "react-static";
import ReactMarkdown from "react-markdown";
export default function Home() {
  const { content } = useRouteData();
  const data = content.length > 0 ? content[0] : {};
  //console.log(content);
  return (
    <div id="home" className="section lb">
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <h1>{data.title}</h1>
            <div>
              <ReactMarkdown source={data.text} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

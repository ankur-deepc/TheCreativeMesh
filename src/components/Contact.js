import React, { Component } from "react";
import ReCAPTCHA from "react-google-recaptcha";
class ContactUS extends Component {
  constructor(props) {
    super(props);
    this.recaptchaRef = React.createRef();
    this.state = {
      captchaValid: false,
      messages: "",
      name: "",
      email: "",
      msg: "",
    };
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    //console.log(name, value);
    this.setState({ [name]: value });
  };
  resetForm = (e) => {
    this.setState({
      messages: "",
      name: "",
      email: "",
      msg: "",
    });
  };
  sendEmail = async (e) => {
    const { name, email, msg } = this.state;
    e.preventDefault();
    //console.log(this.state);
    try {
      const data = await fetch("/.netlify/functions/send-contact-email", {
        method: "POST", // *GET, POST, PUT, DELETE, etc.
        referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify({ name, email, msg }), // body data type must match "Content-Type" header
      });
      this.resetForm();
      const response = data.json();
      this.setState({
        messages: "Your message was sent successfully! We'll be in touch.",
      });
    } catch (error) {}
  };
  onCaptchaChange = (e) => {
    console.log(e);
    this.setState({
      captchaValid: true,
    });
  };
  render() {
    return (
      <div id="contact" className="section db">
        <div className="container-fluid">
          <div className="section-title text-center">
            <h3>Contact</h3>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="contact_form">
                {this.state.messages ? (
                  <div id="messages">{this.state.messages}</div>
                ) : (
                  <form
                    id="contactForm"
                    name="sentMessage"
                    onSubmit={this.sendEmail}
                  >
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <input
                            className="form-control"
                            id="name"
                            type="text"
                            name="name"
                            value={this.state.name}
                            placeholder="Your Name"
                            onChange={this.handleChange}
                            required
                            data-validation-required-message="Please enter your name."
                          />
                          <p className="help-block text-danger"></p>
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <input
                            className="form-control"
                            id="email"
                            type="email"
                            name="email"
                            value={this.state.email}
                            placeholder="Your Email"
                            onChange={this.handleChange}
                            required
                            data-validation-required-message="Please enter your email address."
                          />
                          <p className="help-block text-danger"></p>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="form-group">
                          <textarea
                            className="form-control"
                            id="message"
                            name="msg"
                            value={this.state.msg}
                            onChange={this.handleChange}
                            placeholder="Your Message"
                            required
                            data-validation-required-message="Please enter a message."
                          ></textarea>
                          <p className="help-block text-danger"></p>
                        </div>
                      </div>
                      <div className="clearfix"></div>
                      <div className="col-md-12">
                        <ReCAPTCHA
                          ref={this.recaptchaRef}
                          sitekey="6LeBRuEZAAAAAJXwoPRyY1bLkgBulaf0n5dJ-yNA"
                          onChange={this.onCaptchaChange}
                        />
                      </div>
                      <div className="clearfix"></div>
                      <br />
                      <div
                        className="col-lg-12 "
                        style={{ paddingTop: "30px" }}
                      >
                        <div id="success"></div>
                        <button
                          id="sendMessageButton"
                          className="sim-btn hvr-radial-in"
                          data-text="Send Message"
                          type="submit"
                          disabled={!this.state.captchaValid}
                        >
                          Send Message
                        </button>
                      </div>
                    </div>
                  </form>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ContactUS;

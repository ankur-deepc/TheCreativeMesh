import React, { Component } from "react";
import { Link } from "@reach/router";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { showNav: false };
    this.toggleNav = this.toggleNav.bind(this);
  }
  toggleNav() {
    console.log("shuffleing state");
    this.setState({
      showNav: !this.state.showNav,
    });
  }

  state = {};
  render() {
    const { showNav } = this.state;
    return (
      <nav
        className="navbar navbar-expand-lg navbar-dark fixed-top"
        id="mainNav"
      >
        <div className="container-fluid">
          <Link to="/" className="navbar-brand js-scroll-trigger">
            <img
              className="img-fluid site-logo"
              src="/images/logo_small.png"
              alt=""
            />
          </Link>
          <button
            className="navbar-toggler navbar-toggler-right"
            type="button"
            onClick={this.toggleNav}
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="fa fa-bars"></i>
          </button>
          <div
            className={`collapse navbar-collapse ${showNav ? "show" : ""}`}
            id="navbarResponsive"
          >
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to="/" className="nav-link js-scroll-trigger">
                  <span>Home</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/about" className="nav-link js-scroll-trigger">
                  <span>About</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/gallery" className="nav-link js-scroll-trigger">
                  <span>Gallery</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/team" className="nav-link js-scroll-trigger">
                  <span>Team</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/contact" className="nav-link js-scroll-trigger">
                  <span>Contact Us</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Header;

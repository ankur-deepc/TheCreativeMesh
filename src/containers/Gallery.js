import React from "react";
import { Link } from "@reach/router";
import { useRouteData } from "react-static";

export default function Products() {
  const { totalProducts, products, categories } = useRouteData();
  console.log(totalProducts, products, categories);
  return (
    <div id="gallery" className="section lb">
      <div className="container-fluid">
        <div className="row">
          <div className="col text-center">
            <h1 className="">Gallery</h1>
            <div className="btn-group" role="group" aria-label="Basic example">
              <button type="button" className="btn btn-secondary">
                All ({totalProducts})
              </button>
              {categories.map((category) => (
                <Link
                  key={category.sys.id}
                  to={`/gallery/category/${category.fields.slug}`}
                >
                  <button type="button" className="btn btn-secondary">
                    {category.fields.title}
                    {category.fields.products && (
                      <span>({category.fields.products.length})</span>
                    )}
                  </button>
                </Link>
              ))}
            </div>
          </div>
        </div>
        <div className="gallery-list row">
          {products
            .filter(
              (product) =>
                product.fields.photos && product.fields.photos.length > 0
            )
            .map((product) => (
              <div
                className="col-md-4 col-sm-6 gallery-grid gal_a gal_b"
                key={product.sys.id}
              >
                <Link to={`/products/${product.fields.slug}`}>
                  <div className="gallery-single fix">
                    {product.fields.photos &&
                      product.fields.photos.length > 0 &&
                      product.fields.photos[0].fields && (
                        <img
                          src={`${product.fields.photos[0].fields.file.url}?q=50&w=500&h=500&f=top`}
                          className="img-fluid"
                          alt="Image"
                        />
                      )}
                    <div className="img-overlay">
                      <h3>{product.fields.title}</h3>
                    </div>
                  </div>
                </Link>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}

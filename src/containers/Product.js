import React from "react";
import { useRouteData } from "react-static";
import { Link } from "components/Router";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import ReactMarkdown from "react-markdown";
import Price from "react-price";

export default function Post() {
  const { product } = useRouteData();
  const { fields } = product;
  const { photos } = fields;
  //console.log(JSON.stringify(fields.photos));
  return (
    <div className="container-fluid">
      <br />
      <br />
      <div className="row">
        <div className="col-12 col-sm-6">
          <Carousel showArrows={true}>
            {photos.map((photo) => (
              <div key={photo.sys.id}>
                <img
                  src={`${photo.fields.file.url}?q=90&w=500&h=500&f=top`}
                  alt="Image"
                />
                {photo.fields.title && (
                  <p className="legend">{photo.fields.title}</p>
                )}
              </div>
            ))}
          </Carousel>
        </div>
        <div className="col-12 col-sm-6">
          <h2>{fields.title}</h2>
          {fields.description && (
            <div className="lead">
              <ReactMarkdown source={fields.description} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

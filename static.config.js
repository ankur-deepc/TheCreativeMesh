import path from "path";
import getClient from "./src/lib/get-client";
import dotenv from "dotenv";
dotenv.config();

export default {
  getRoutes: async () => {
    //return [];
    const { items: pages } = await getClient(process.env).getEntries({
      content_type: "pages",
    });
    //    console.log("------pages----");
    //console.log(pages);
    const aboutContent = pages
      .filter((page) => page.fields.name.toLowerCase() === "about")
      .map((page) => page.fields);
    const homeContent = pages
      .filter((page) => page.fields.name.toLowerCase() === "home")
      .map((page) => page.fields);
    const { items: products } = await getClient(process.env).getEntries({
      content_type: "product",
    });
    //console.log("------products----");
    //console.log(products);
    const { items: teams } = await getClient(process.env).getEntries({
      content_type: "team",
    });
    //console.log("------teams----");
    //console.log(teams);
    const { items: categories } = await getClient(process.env).getEntries({
      content_type: "category",
    });
    //console.log("------categories----");
    //console.log(categories);

    return [
      {
        path: "/",
        getData: () => ({
          content: homeContent,
        }),
      },
      {
        path: "/about",
        getData: () => ({
          content: aboutContent,
        }),
      },
      {
        path: "/team",
        getData: () => ({
          teams,
        }),
      },
      {
        path: "/gallery",
        template: "src/containers/Gallery",
        getData: () => ({
          totalProducts: products.length,
          products,
          categories,
        }),
        children:
          categories && categories.length > 0
            ? categories.map((category) => ({
                path: `/category/${category.fields.slug}`,
                template: "src/containers/Category",
                getData: () => ({
                  totalProducts: products.length,
                  products: category.fields?.products
                    ? category.fields.products
                    : [],
                  category,
                  categories,
                }),
              }))
            : [],
      },
      {
        path: "/products",
        template: "src/containers/Gallery",
        getData: () => ({
          products,
          categories,
        }),
        children:
          products && products.length > 0
            ? products.map((product) => ({
                path: `/${product.fields.slug}`,
                template: "src/containers/Product",
                getData: () => ({
                  product,
                }),
              }))
            : [],
      },
    ];
  },
  plugins: [
    [
      require.resolve("react-static-plugin-source-filesystem"),
      {
        location: path.resolve("./src/pages"),
      },
    ],
    require.resolve("react-static-plugin-reach-router"),
    require.resolve("react-static-plugin-sitemap"),
  ],
};

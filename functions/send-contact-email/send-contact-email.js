require("dotenv").config();
const {
  MAILGUN_API_KEY,
  MAILGUN_DOMAIN,
  MAILGUN_URL,
  CONTACT_TO_EMAIL_ADDRESS,
} = process.env;
const mailgun = require("mailgun-js")({
  apiKey: "22de0f5962856fb5dc8f7c63a6f9cd2c-ba042922-e77548f4",
  domain: "sandbox37fd55e7e95e4fc3ba32cc5c40fbf0d0.mailgun.org",
  url:
    "https://api.mailgun.net/v3/sandbox37fd55e7e95e4fc3ba32cc5c40fbf0d0.mailgun.org",
});

exports.handler = async (event) => {
  if (event.httpMethod !== "POST") {
    return {
      statusCode: 405,
      body: "Method Not Allowed",
      headers: { Allow: "POST" },
    };
  }

  const data = JSON.parse(event.body);
  const { name, email, msg } = data;
  if (!msg || !name || !email) {
    return { statusCode: 422, body: "Name, email, and message are required." };
  }

  const mailgunData = {
    from: email,
    to: "speedyankur@gmail.com,swatisharma.15011989@gmail.com",
    "h:Reply-To": email,
    subject: `SHIVANI-NETLIFY : New contact from ${name}`,
    text: `Name: ${name}\nEmail: ${email}\nMessage: ${msg}`,
  };

  return mailgun
    .messages()
    .send(mailgunData)
    .then(() => ({
      statusCode: 200,
      body: "Your message was sent successfully! We'll be in touch.",
    }))
    .catch((error) => ({
      statusCode: 422,
      body: `Error: ${error}`,
    }));
};
